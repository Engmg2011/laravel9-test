<?php
namespace App\Traits;

use App\Models\Locale;

trait Localizable{
    public function locales(){
        return $this->morphMany(Locale::class , 'localizable');
    }
}
