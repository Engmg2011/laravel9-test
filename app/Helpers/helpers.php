<?php


if(!function_exists('array_only')){
    function array_only (Array $array , Array $keys): Array {
        return array_filter($array, function($key) use ($keys) {
            return in_array($key, $keys);
        },ARRAY_FILTER_USE_KEY);
    }
}





