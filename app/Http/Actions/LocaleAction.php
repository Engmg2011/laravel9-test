<?php

namespace App\Http\Actions;

use App\Models\Locale;
use Illuminate\Database\Eloquent\Model;

class LocaleAction
{

    public function process(array $data)
    {
        return array_only($data, ['name', 'description', 'locale', 'localizable_type', 'localizable_id']);
    }

    public function setLocales(Model $model, array $locales)
    {
        foreach ($locales as &$locale) {
            $localeData = $this->process($locale);
            $localeData += [
                'localizable_id' => $model->id,
                'localizable_type' => get_class($model),
                'locale' => $locale['locale'] ?? \App::getLocale()
            ];
            if (!isset($locale['id']))
                Locale::insert($localeData);
            else
                Locale::find($locale->id)->update($localeData);
        }
    }
}
