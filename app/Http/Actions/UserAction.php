<?php

namespace App\Http\Actions;

use App\Models\User;

class UserAction
{
    public function __construct(public LocaleAction $localeAction)
    {
    }

    public function process(array $data)
    {
        if(isset($data['password']))
            $data['password'] = bcrypt('password');

        return array_only($data, ['name', 'email', 'password']);
    }

    public function get($id)
    {
        return User::with('locales')->find($id);
    }

    public function set(array $data)
    {
        $modelData = $this->process($data);

        $model = (!isset($data['id'])) ?
            User::create($modelData) :
            tap(User::find($data['id']))->update($modelData);

        if ($data['locales'])
            $this->localeAction->setLocales($model, $data['locales']);
        return $model;
    }
}
