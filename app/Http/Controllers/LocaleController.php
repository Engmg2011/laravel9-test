<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Database\Eloquent\Model;

class LocaleController extends Controller
{
    public function __construct(protected App\Http\Actions\LocaleAction $action)
    {
    }

    public function setLocales(Model $model , array $locales)
    {
        return $this->action->setLocales($model, $locales);
    }
}
