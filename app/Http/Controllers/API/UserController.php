<?php

namespace App\Http\Controllers\API;

use App\Http\Actions\UserAction;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function __construct(public UserAction $action)
    {
    }

    public function show($id)
    {
        return response()->json($this->action->get($id));
    }
    public function update($id)
    {
       $user = $this->action->set(request()->all() + compact('id'));
        return response()->json($user);
    }
}
