<?php

namespace App\Http\Controllers;

use App\Http\Actions\UserAction;

class UserController extends Controller
{
    public function __construct(public UserAction $action)
    {
    }

    public function update()
    {
        $this->action->set(request()->all());
        return view('welcome');
    }
}
